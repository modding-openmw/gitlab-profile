# Modding-OpenMW

Welcome!

This is a [free software](https://en.wikipedia.org/wiki/Free_software) organization dedicated to modding [Morrowind](https://en.wikipedia.org/wiki/The_Elder_Scrolls_III:_Morrowind) (and other games) with [the OpenMW engine](https://openmw.org/).

Some key places to check if you're unsure about where to get started are:

* The Modding-OpenMW.com [website](https://modding-openmw.com/) and [source code](https://gitlab.com/modding-openmw/modding-openmw.com)
* The mod [Update Center](https://modding-openmw.gitlab.io/update-center/) and [source code](https://gitlab.com/modding-openmw/update-center)
* Our curated [mod template repo](https://gitlab.com/modding-openmw/momw-mod-template) that you can fork for a nice project base complete with GitLab-hosted static website and automated zip publishing capabilities build right in.
